#!/bin/bash

# Runs the donation analysis
python ./src/donation-analytics.py -i ./input/itcont.txt ./input/percentile.txt -o ./output/repeat_donors.txt

