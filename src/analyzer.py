import heapq
import math

import util


class PercentileHeap:
	def __init__(self,percentile=50):
		# heapq automatically uses a min heap, so we'll invert values by -1 that are added to the self.max_heap to simulate a max heap
		
		self.max_heap = [] # elements <=
		self.min_heap = [] # elements > 

		self.percentile = percentile 
		self.size = 0
		self.total_sum = 0


	def fetch_percentile_element(self):

		if(self.size == 0):
			return None

		percentile_index = math.ceil(self.size * self.percentile * 1.0 / 100)
		
		return int(round(self.max_heap[0] * -1))


	def add(self,amount):
		self.size += 1
		self.total_sum += amount
		percentile_index= math.ceil(self.size * self.percentile * 1.0 / 100)

		if(self.size == 1 or amount <= self.max_heap[0] * -1):
			heapq.heappush(self.max_heap, amount * -1)
		else:
			heapq.heappush(self.min_heap, amount)

		# Transfer elements to balance heap
		if(len(self.max_heap) > percentile_index):
			# Pop off max from this heap and move to other heap
			transfer_element = heapq.heappop(self.max_heap) * -1
			heapq.heappush(self.min_heap,transfer_element)

		elif(len(self.max_heap) < percentile_index):
			transfer_element = heapq.heappop(self.min_heap)
			heapq.heappush(self.max_heap, transfer_element * -1)


	def phprint(self):
		print(	
			"Max Heap: {}".format(self.max_heap) + "\n"
			"Min Heap: {}".format(self.min_heap) + "\n"
			"Size: {}, Total Sum: {}".format(self.size,self.total_sum) + "\n"
		)



class DonationAnalyzer:


	def __init__(self, percentile):	
		''' Dictionary/hash for processing contributor information
			Key : (donor name, zip)
			Value : { transaction information for earliest known donation by donor }
		'''
		self.cmap = {}
		''' Dictionary/hash storing repeat donor information, updated every time a repeat donor is identified
			Key: (recipient,zip,year) 
			Value: PercentileHeap that stores numerical values of contributions
		'''
		self.rdmap = {}
		self.percentile = percentile

	''' Triggered upon identifying a repeat donor, 
		Returns relevant donation information in the form of a list'''
	def process_repeat_donor(self,uid,ut):
		# Figure out which transaction is the repeat donor transaction, since transactions not guarenteed to come in order
		old_ut = self.cmap[uid]

		if(ut["transaction_dt"] < old_ut["transaction_dt"]):
			rut = old_ut
			self.cmap[uid] = ut
		else:
			rut = ut
	
		# Examine rdmap and outputs relevant repeating donor stats
		rdid = (rut["cmte_id"],rut["zip_code"],rut["year"])
		
		if(rdid in self.rdmap):
			self.rdmap_update(rdid,rut)
		else:
			self.rdmap_add(rdid,rut)

		return self.rdmap_report(rdid,rut)


	''' Updates a value in rdmap'''
	def rdmap_update(self,rdid,rut):
		# only need transaction sum to be stored along the heap
		ph = self.rdmap[rdid]
		ph.add(rut["transaction_amt"])


	''' Adds a value to rdmap'''
	def rdmap_add(self,rdid,rut):
		ph = PercentileHeap(self.percentile)
		ph.add(rut["transaction_amt"])
		self.rdmap[rdid] = ph
		
		
	''' Fetches donation report, return string with result'''
	def rdmap_report(self,rdid,rut):
		ph = self.rdmap[rdid]
		rd_output = [
			rdid[0],
			rdid[1],
			rdid[2],
			ph.fetch_percentile_element(),
			ph.total_sum,
			ph.size
		]

		rd_string = "|".join(map(lambda x: str(x), rd_output))

		return rd_string

	