import sys
import argparse

import util
import analyzer

def main():

	# Retrieve input files and output files from command line args
	parser = argparse.ArgumentParser(description="Insight Coding Challenge: Donation Analytics.")
	parser.add_argument("-i",action="store",metavar='filepath',nargs=2,help="input filepaths: must be entered in order as donor record file, percentile file")
	parser.add_argument("-o",action="store",metavar='filepath',help="output filepath")
	args = parser.parse_args()


	# Handles input stream/output stream and performs analysis
	setup = util.setup(args)
	if(setup == "good"):

		ofilepath = args.o
		itcont_filepath = args.i[0]
		percentile_filepath = args.i[1]


		# Get percentile from file
		percentile = None
		with open(percentile_filepath) as p:
			pdata = p.readline().strip()
			if(pdata.isdigit() and int(pdata) >= 0 and int(pdata) <= 100):
				percentile = int(pdata)
		if  percentile is None:
			print("Error: invalid percentile found in percentile file.")
			sys.exit(1)


		# Create DonationAnalyzer
		da = analyzer.DonationAnalyzer(percentile)


		# Open output stream 
		o = open(ofilepath, 'w');
		# Process itcont file line by line
		with open(itcont_filepath) as itcont:
			for line in itcont:
				uid, ut = util.parse_contribution(line)
				
				if( uid is not None and ut is not None):
					if(uid in da.cmap):
						result = da.process_repeat_donor(uid,ut)
						# Write to stdout and write to file
						o.write(result + "\n")
						
					else:
						da.cmap[uid] = ut
		o.close()


	else:
		print("Error: Could not run analysis.\nPlease double check program setup (input filepaths, output filepaths).")


if(__name__ == "__main__"):
	main()

