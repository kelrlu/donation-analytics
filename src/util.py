import datetime
import os

''' Parses and preprocesses a line of text with contribution information
	Returns uid, ut (user name, contribution information) if valid contribution
	Returns None, None if invalid contribution

	Raw Data Format
		* 	0 CMTE_ID
			1 AMNDT_IND
			2 RPT_TP
			3 TRANSACTION_PGI
			4 IMAGE_NUM
			5 TRANSACTION_TP
			6 ENTITY_TP
		* 	7 NAME
			8 CITY
			9 STATE
		* 	10 ZIP_CODE
			11 EMPLOYER
			12 OCCUPATION
		**	13 TRANSACTION_DT
		** 	14 TRANSACTION_AMT
		** 	15 OTHER_ID
			16 TRAN_ID
			17 FILE_NUM
			18 MEMO_CD
			19 MEMO_TEXT
			20 SUB_ID

		Desired Attributes:
			CMTE_ID, NAME, ZIP_CODE, TRANSACTION_DT, TRANSACTION_AMT, OTHER_ID
'''
def parse_contribution(c):
	data = c.split("|")

	if(len(data) != 21):
		return [None, None]

	# Ut contains all relevant information about a transaction
	ut = {}
	ut["cmte_id"] = data[0]
	ut["name"] = data[7]
	ut["zip_code"] = data[10]
	ut["transaction_dt"] = get_date(data[13])
	ut["transaction_amt"] = data[14]
	ut["other_id"] = data[15]

	# Check data validity
	if any ([ 	ut["other_id"] != "",
				ut["transaction_dt"] is None,
				len(ut["zip_code"]) < 5,
				ut["name"] == "",
				ut["cmte_id"] == "",
				ut["transaction_amt"] == "" or ut["transaction_amt"] is None,
				not ut["zip_code"].isdigit(),
				not is_float(ut["transaction_amt"])

			]):
		return [None, None]

	ut["zip_code"] = data[10][:5]
	ut["year"] = int(data[13][4:8])
	ut["transaction_amt"] = float(data[14])

	uid = (ut["name"], ut["zip_code"])

	return [uid, ut]


def is_float(e):
	try:
		float(e)
		return True
	except ValueError:
		return False


def get_date(d):
	if(d is None or len(d) != 8):
		return None
	try:
		month = int(d[:2])
		date = int(d[2:4])
		year = int(d[4:8])
		date = datetime.date(year, month, date)
		return date
	except:
		return None



# Checks arguments to the program
def setup(args):
	if all([ 	args.i is not None,
				os.path.exists(args.i[0]), # Itcont file exists
				os.path.exists(args.i[1]), # Percentile file exists
				os.path.isdir(os.path.dirname(args.o)), # Output file directory exists

	]):
		return "good"
	return "error"
