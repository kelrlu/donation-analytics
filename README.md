Kellie Lu
krl2130@columbia.edu
Insight Coding Challenge

EDITED: ADDENDEDUM AS OF 2/20/17

Hi Insight Committee,
In the small chance you haven't get gotten around to this submission yet, I added a belated edit on 2/20/2018(submitted on date 2/13/2018) after the interview preparation session with David upon discovering that live Github repos were still examined. This was a mistake I discovered early the following day but felt that since the code had already been submitted, there was no chance to correct it:/ 

The latest commit alters where transaction amount is rounded in the code. The old code had the rounding occur in the preprocessing step, but new code only has transaction amount being rounded when percentile amount is being fetched. 

Because amounts were previously rounded during preprocessing, I believe that the total amount may have been wrong for many outputs (e.g. Given these amounts $0.4 + $1.2 = $1.7, but old code would have rounded $0 + $1 = $1).

==================================================
Software Dependencies/ Pre-requisites 
==================================================
	Python 3.6.4

	pip3 install argparse datetime


==================================================
Assumptions
==================================================

	If a donor donates multiple donations within the same year, those multiple donations are considered repeat donations. 
	E.g. Joe's first ever donation is 07/13/1995. Joe then donates again 08/13/1995. The second donation is counted as a repeat donor donation for the calendar year 1995. 



==================================================
Implementation/ Solution
==================================================

Preprocessing: (parse_contribution in util.py)
	
	As the program receives a line of text representing a transaction, it first preprocesses the line to determine if it is valid and then extracts relevant information in the form of a dictionary. 

	Transactions amounts are automatically rounded up to the nearest dollar in this process.

DonationAnalyzer: (DonationAnalyzer in analyzer.py)
	
	The transaction is sent to a donation analyzer, which primarily consists of two dictionaries, cmap and rdmap.
	
	cmap is a hash map with < donor_id, earliest_transaction_info >  <key, value> set up, where keys are (name, zipcode) pair identifying the donor and values are a dictionary containing transaction information about the earliest known transaction from that donor.

	Transactions are added to the cmap dictionary until a collision occurs.
	If a collision occurs with a key, there exists a repeat donor transaction. You fetch the colliding values from the dictionary, compare dates between transactions, update cmap with earlier transaction and identify the later transaction as the repeat donor transaction.

	The repeat donor transaction is then sent to the second dictionary rdmap, which is a hash map with < (recipient, zip code, year), PercentileHeap >  <key, value> set up, where keys are the identifying values we want to use to organize donation data and values are a custom data structure called a PercentileHeap used for efficient calculation and access to desired analysis information.

	PercentileHeap is a double heap data structure storing a max_heap, min_heap, and given percentile number allowing for O(1) access to the desired percentile value for transaction amount given the particular key. Transaction amount values that fall under the given percentile are stored in the max_heap, while transaction amount values that fall above the given percentile are stored in the min_heap. 

	When a repeat transaction is detected, its transaction amount value is added to its corresponding PercentileHeap for (recipient, zip code, year) and the PercentileHeap then self balances (identifying which heap the amount belongs to, making sure total percentage of the number of transactions in the max_heap is less than the given percentile by comparing to calculated nearest rank number).

	In general, this operation should be O(log(n)) to insert an element into one of the two heaps, but cases where an element must transfer heaps (heap pop from max_heap, heap insert into min_heap) are prone to occur and would add to the time complexity. This case would occur when the size of max_heap does not equal the percentile ordinal rank for the current number of transactions.

	The PercentileHeap also conviniently keeps track of the total number of transactions and total amount donated as it is updated.



Test Suite:
	
	Test information (what part of the program the test examines) can be found in the README.md file of every test1,test2,test3,etc. folder.


==================================================
Additional
==================================================
Potential Issues:
	
	Since the program uses hash tables, there is potential for there to be large space complexity,especially with input data that is large and sparse (few repeat donor transactions, many transactions total). 


Test Results on a Large Dataset:
	
	The program was also run on the 2015-2016 Contributions by Individuals (3.83 GB) datasetfor examination, and "time ./run.sh" outputted the following.
		
	real	10m16.908s
	user	9m22.013s
	sys		0m28.571s



