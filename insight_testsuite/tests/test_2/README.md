Test to confirm that invalid transactions are correctly filtered from being analyzed.
These include:
    - Empty TRANSACTION_DT
    - Invalid TRANSACTION_DT
    - Empty ZIP_CODE
    - Invalid ZIP_CODE
    - Empty NAME
    - Empty TRANSACTION_AMT
    - Empty CMTE_ID
    - Nonempty OTHER_ID
