Test to confirm percentiles values are being calculated correctly within the PercentileHeap.

Amounts are added (100,200,300,400,500,600,700,800,900,1000) to the same rdmap key, examine the percentile calculation that emerges with each add